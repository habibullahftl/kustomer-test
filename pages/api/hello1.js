// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
const axios = require('axios')
async function handler(req, res) {
  const options = {
    // url: 'https://api.kustomerapp.com/v1/kb/articles/63325f805a253d23ed6fb519',
    url: 'https://api.kustomerapp.com/v1/kb/articles/search?id=',
    method: 'GET',
    headers: {
      'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzMzJiM2E2YTIyZmVjMTg4MTI0ZDAwYyIsInVzZXIiOiI2MzMyYjNhNDRjNGQ2N2I1YzFjM2IzM2IiLCJvcmciOiI2MzMyNWRjNjVjNzY2ZDNhODE2OWVkYzIiLCJvcmdOYW1lIjoiYWxnb3NvbHZlci1sbGMiLCJ1c2VyVHlwZSI6Im1hY2hpbmUiLCJwb2QiOiJwcm9kMSIsInJvbGVzIjpbIm9yZy51c2VyLmtiLnJlYWQiLCJvcmcucGVybWlzc2lvbi5rYi5yZWFkIl0sImV4cCI6MTY2NDg3MTk2OSwiYXVkIjoidXJuOmNvbnN1bWVyIiwiaXNzIjoidXJuOmFwaSIsInN1YiI6IjYzMzJiM2E0NGM0ZDY3YjVjMWMzYjMzYiJ9.6X7yHhOnAAR75Xy4FH6yBRQA5yIvz56ZdVtu_TZYg1k',
      'Content-Type': 'application/json',
    }
  };

  const data = await axios(options)

  res.status(200).send(data.data.data[0])

}
export default handler;
