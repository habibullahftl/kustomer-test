import axios from "axios";
import React, { useEffect, useState } from "react";
import Script from 'next/script'

const Test = () => {

// const options = {
//     url: 'https://api.kustomerapp.com/v1/audit-logs',
//     method: 'GET',
//     headers: {
//       'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzMzI3ODZlZTg4ZDc4ZDI4YmUwYTE1MyIsInVzZXIiOiI2MzMyNzg2Y2Q2OWViMzQ1ZmU2ODNkZDciLCJvcmciOiI2MzMyNWRjNjVjNzY2ZDNhODE2OWVkYzIiLCJvcmdOYW1lIjoiYWxnb3NvbHZlci1sbGMiLCJ1c2VyVHlwZSI6Im1hY2hpbmUiLCJwb2QiOiJwcm9kMSIsInJvbGVzIjpbIm9yZy51c2VyLmF1ZGl0X2xvZ3MucmVhZCIsIm9yZy5wZXJtaXNzaW9uLmF1ZGl0X2xvZ3MucmVhZCJdLCJleHAiOjE2NjQ4NTY4MTAsImF1ZCI6InVybjpjb25zdW1lciIsImlzcyI6InVybjphcGkiLCJzdWIiOiI2MzMyNzg2Y2Q2OWViMzQ1ZmU2ODNkZDcifQ.hSw7w_7oCvfWKINE7pGVglJ_lsBdKCfUov6dwlIsdPI',
//       'Content-Type':'application/json',
//     }
//   };
  
//   axios(options)
//     .then(response => {
//       console.log(response.data);
//     });

const [articleData, setArticleData] = useState({});

useEffect(()=>{
    axios.get('/api/hello')
.then(res=>{
    console.log(res.data);
    setArticleData(res.data);
})
},[])
  return <div style={{
    background: "#ffffff"
  }}>
<button id="startChatButton">Start Chat Button</button>
<Script
  id="63325dca8894e51dcb93f0a1" 
  strategy="afterInteractive"
  dangerouslySetInnerHTML={{
    __html: `
    console.log(Kustomer)
    Kustomer.start({
        brandId: '63325dca8894e51dcb93f0a1'
      });
  `,
  }}
/>
{articleData?.attributes?.htmlBody ? <div  dangerouslySetInnerHTML={{ __html: articleData.attributes.htmlBody }} />:''}
  </div>;
};

export default Test;
