/* eslint-disable @next/next/no-sync-scripts */
import { Html, Head, Main, NextScript } from 'next/document'
import Script from 'next/script'

export default function Document() {
  return (
    <Html>
      <Head />
      <body>
        <Main />
        <NextScript />
        
    {/* <Script  strategy="beforeInteractive" src="https://cdn.kustomerapp.com/chat-web/widget.js" data-kustomer-api-key='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzMzI5ZmE4ODY1NmEyOTgyNDMwMDg3NyIsInVzZXIiOiI2MzMyOWZhN2IzMThjYTMxMDA3MjQ3MDEiLCJvcmciOiI2MzMyNWRjNjVjNzY2ZDNhODE2OWVkYzIiLCJvcmdOYW1lIjoiYWxnb3NvbHZlci1sbGMiLCJ1c2VyVHlwZSI6Im1hY2hpbmUiLCJwb2QiOiJwcm9kMSIsInJvbGVzIjpbIm9yZy50cmFja2luZyJdLCJhdWQiOiJ1cm46Y29uc3VtZXIiLCJpc3MiOiJ1cm46YXBpIiwic3ViIjoiNjMzMjlmYTdiMzE4Y2EzMTAwNzI0NzAxIn0.NvEMULofGQKnF0TdKaU0eaChNVtVQfMmfXLhkAznrSM'  /> */}
    <script  strategy="beforeInteractive" src="https://cdn.kustomerapp.com/chat-web/widget.js" data-kustomer-api-key='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzMzI5ZmE4ODY1NmEyOTgyNDMwMDg3NyIsInVzZXIiOiI2MzMyOWZhN2IzMThjYTMxMDA3MjQ3MDEiLCJvcmciOiI2MzMyNWRjNjVjNzY2ZDNhODE2OWVkYzIiLCJvcmdOYW1lIjoiYWxnb3NvbHZlci1sbGMiLCJ1c2VyVHlwZSI6Im1hY2hpbmUiLCJwb2QiOiJwcm9kMSIsInJvbGVzIjpbIm9yZy50cmFja2luZyJdLCJhdWQiOiJ1cm46Y29uc3VtZXIiLCJpc3MiOiJ1cm46YXBpIiwic3ViIjoiNjMzMjlmYTdiMzE4Y2EzMTAwNzI0NzAxIn0.NvEMULofGQKnF0TdKaU0eaChNVtVQfMmfXLhkAznrSM'  ></script>
      </body>
    </Html>
  )
}